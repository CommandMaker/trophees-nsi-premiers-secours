<?php

namespace Tests\Unit\Domain\User;

use App\Domain\User\UserService;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    use RefreshDatabase;

    private array $credentials = [
        'name' => 'John Doe',
        'email' => 'john@doe.fr',
        'password' => 'johndoe1234',
        'confirm_password' => 'johndoe1234'
    ];

    public function testIfServiceRegisterUser(): void
    {
        $userService = new UserService();

        $this->assertInstanceOf(User::class, $userService->registerUser($this->credentials));
    }
}
