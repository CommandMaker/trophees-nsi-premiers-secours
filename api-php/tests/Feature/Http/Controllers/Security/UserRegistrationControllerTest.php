<?php

namespace Tests\Feature\Http\Controllers;

use App\Domain\User\Events\UserRegisteredEvent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class UserRegistrationControllerTest extends TestCase
{
    use RefreshDatabase;

    private array $credentials = [
        'name' => 'John Doe',
        'email' => 'john@doe.fr',
        'password' => 'johndoe1234',
        'confirm_password' => 'johndoe1234'
    ];

    public function testIfUserIsSucessfullyRegistered(): void
    {
        Event::fake();

        $response = $this->postJson('/api/user/register', $this->credentials);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => 'ok',
            'data' => [
                'name' => 'John Doe'
            ]
        ]);

        Event::assertDispatched(UserRegisteredEvent::class);

        $this->assertDatabaseCount('users', 1);
    }

    public function testIfFailWhenCredentialsAreInvalid(): void
    {
        $response = $this->postJson('/api/user/register', [$this->credentials['name'], $this->credentials['password']], [
            'Content-Type' => 'application/json'
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The name field is required. (and 3 more errors)'
        ]);

        $this->assertDatabaseCount('users', 0);
    }
}
