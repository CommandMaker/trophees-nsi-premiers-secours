<?php

namespace Tests\Feature\Http\Controllers\Security;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserLoginControllerTest extends TestCase
{

    use RefreshDatabase, WithFaker;

    public function testIfLoginSuccessfully(): void
    {
        $user = User::factory()->create([
            'password' => 'johndoe1234'
        ]);

        $response = $this->postJson('/api/user/login', [
            'email' => $user->email,
            'password' => 'johndoe1234',
            'device_name' => $this->faker->sentence()
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'status' => 'ok'
        ]);

        $this->assertDatabaseCount('personal_access_tokens', 1);
    }

    public function testIfFailWhenSubmittingInvalidCredentials(): void
    {
        $user = User::factory()->create();

        $response = $this->postJson('/api/user/login', [
            'email' => $user->email,
            'password' => 'aFalsePassword',
            'device_name' => $this->faker()->sentence()
        ]);

        $response->assertStatus(422);
    }

    public function testIfLogoutSuccessfully(): void
    {
        $user = User::factory()->create([
            'password' => 'johndoe1234'
        ]);

        $login = $this->postJson('/api/user/login', [
            'email' => $user->email,
            'password' => 'johndoe1234',
            'device_name' => $this->faker()->sentence()
        ]);

        $token = $login->json('token');

        $response = $this->post('/api/user/logout', headers: [
            'Authorization' => "Bearer $token"
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseCount('personal_access_tokens', 0);
    }
}
