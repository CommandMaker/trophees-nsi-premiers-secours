# API

L'API permet la communication entre la plateforme d'entraînement et la plateforme d'utilisation, afin de proposer des exercices en fonction des interventions réalisées en cas réel

## Développement
L'API est développée sous Laravel 10.4 et PHP 8.3 afin de gagner du temps lors de sa conception, car ce n'est pas la partie la plus importante du projet.

Prérequis
- PHP 8.3 (minimum)
- Composer
- En cas de mise en production, un serveur de mail (MailHog par exemple pour du test local)

```bash
# Installation des dépendances avec Composer
composer install
# Lancement du serveur interne de PHP (par défaut sur le port 8000)
php -S 0.0.0.0:8000 -t public
```
