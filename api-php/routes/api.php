<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\Security\UserLoginController;
use App\Http\Controllers\Security\UserRegistrationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/hello-world', [HomeController::class, 'home'])
    ->name('home.greetings');

/* Register & login */
Route::post('/user/register', [UserRegistrationController::class, 'registerUser'])
    ->name('security.user.register');

Route::post('/user/login', [UserLoginController::class, 'login'])
    ->name('security.user.login');

Route::post('/user/logout', [UserLoginController::class, 'logout'])
    ->middleware('auth:sanctum')
    ->name('security.user.logout');
/* /Register & login */

Route::get('/user/info', function () {
    return auth()->user();
})
    ->middleware('auth:sanctum')
    ->name('security.user.dump');
