<?php

namespace App\Http\Controllers\Security;

use App\Domain\User\UserService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Security\UserRegistrationRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserRegistrationController extends Controller
{
    public function registerUser(UserRegistrationRequest $request, UserService $userService): JsonResponse
    {
        $user = $userService->registerUser($request->all());

        return response()->json([
            'status' => 'ok',
            'data' => $user
        ]);
    }
}
