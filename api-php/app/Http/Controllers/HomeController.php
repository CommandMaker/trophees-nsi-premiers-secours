<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(): JsonResponse
    {
        return response()->json([
            'status' => 'ok',
            'message' => 'Hello World'
        ]);
    }
}
