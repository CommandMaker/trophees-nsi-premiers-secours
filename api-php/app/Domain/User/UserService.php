<?php

namespace App\Domain\User;

use App\Domain\User\Events\UserRegisteredEvent;
use App\Models\User;

/**
 * Perform operations on User
 */
class UserService {

    /**
     * Register a new user in the system
     */
    public function registerUser(array $credentials): User
    {
        $user = User::create($credentials);

        event(new UserRegisteredEvent($user));

        return $user;
    }
}
