<?php

namespace App\Domain\User\Events;

use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     */
    public function __construct(
        protected User $user
    )
    {
    }

    /**
     * Return the user associated with the event
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
