import PyQt6.QtWidgets as Qt


class MainWindow(Qt.QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle('Hello World')
        self.resize(975, 625)
        centralWidget = Qt.QWidget()
        layout = Qt.QVBoxLayout()

        button = Qt.QPushButton('Hello World')
        button.clicked.connect(self.button_click)

        layout.addWidget(button)
        centralWidget.setLayout(layout)
        self.setCentralWidget(centralWidget)

    def button_click(self):
        print('Clicked !')
