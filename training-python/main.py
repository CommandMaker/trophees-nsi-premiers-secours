import PyQt6.QtWidgets as Qt
import sys

from mainwindow import MainWindow


app = Qt.QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()
